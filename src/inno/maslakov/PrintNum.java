package inno.maslakov;

import java.util.HashMap;
import java.util.Random;

/**
 * Created by sa on 13.02.17.
 */
public class PrintNum implements Runnable {
    private HashMap<Integer, Integer> nums = new HashMap<>();
    private GenRand genRand = new GenRand();
    private boolean flagRun = true;

    public PrintNum() {
        Thread threadGenNum = new Thread(genRand);
        threadGenNum.start();
    }

    @Override
    public void run() {
        try {
            while(flagRun) {
                flagRun = genRand.isFlagRun();
                System.out.println(genRand.getNums());
                Thread.sleep(5000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
