package inno.maslakov;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by sa on 13.02.17.
 */
public class GenRand implements Runnable{
    private volatile HashMap<Integer, Integer> nums = new HashMap<>(50);
    private boolean flagRun = true;
    @Override
    public void run() {

        while(flagRun) {
            try {
                genRand();
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void genRand(){
        Random random = new Random();
        Integer randInt = random.nextInt(100);
        System.out.println("RandNum = " + randInt);
        if (nums.containsKey(randInt)){
            nums.put(randInt, nums.get(randInt) + 1);
            if (nums.get(randInt) > 5){
                flagRun = false;
            }
        }
        else {
            nums.put(randInt, 1);
        }
    }

    public HashMap<Integer, Integer> getNums(){
        return nums;
    }

    public int getOneRandNum(){
        Random random = new Random();
        return random.nextInt();
    }

    public boolean isFlagRun() {
        return flagRun;
    }
}
